<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
        <ul>
            <li>
            <a href="https://creative-tim.com/presentation">
                {{ __('About Us') }}
            </a>
            </li>
            <li>
            <a href="http://blog.creative-tim.com">
                {{ __('Blog') }}
            </a>
            </li>
        </ul>
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script>, created with<i class="material-icons">favorite</i>by
         <a>Aritz, Mikel & Iker</a> 
    </div>
  </div>
</footer>